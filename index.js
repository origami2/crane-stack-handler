function CraneStackHandler(stack) {
  if (!stack) throw new Error('stack is required');

  return function (socket, crane) {
    stack.listenSocket(socket);
  };
}

module.exports = CraneStackHandler;
