var CraneStackHandler = require('..');
var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('CraneStackHandler', function () {
  it('stack is required', function () {
    assert
    .throws(
      function () {
        new CraneStackHandler();
      },
      /stack is required/
    );
  });

  it('returns a function', function () {
    var handler = new CraneStackHandler({});

    assert(typeof(handler) === 'function');
  });

  it('passes socket to stack', function (done) {
    var craneSocket = new EventEmitter();

    var handler = new CraneStackHandler({
      listenSocket: function (socket) {
        try {
          assert.equal(craneSocket, socket);

          done();
        } catch (e) {
          done(e);
        }
      }
    });

    handler(craneSocket, {});
  });
});
